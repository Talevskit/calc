package com.example.calc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.calc.service.CalcService;

@RestController
@RequestMapping("/calculator")
public class CalcController {
	
	@Autowired
	private CalcService calcService;
	
	@GetMapping
	public double calculate(@RequestParam Integer a,
			@RequestParam Integer b, @RequestParam String opr) throws Exception {
		return calcService.calculate(a, b, opr);
	}
}
