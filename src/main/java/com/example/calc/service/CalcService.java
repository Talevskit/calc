package com.example.calc.service;

import org.springframework.stereotype.Service;

@Service
public class CalcService {

	public double calculate(Integer a, Integer b, String oper) throws Exception {
		switch(oper) {
		case "+":return a+b;
		case "-":return a-b;
		case "/":return a/b;
		case "*":return a*b;
		default: 
			throw new Exception();
		}
	}
}